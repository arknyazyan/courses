export { default as UiButton } from '../../components/UiButton.vue'
export { default as UiDataTable } from '../../components/UiDataTable.vue'
export { default as UiInputField } from '../../components/UiInputField.vue'
export { default as UiPaginator } from '../../components/UiPaginator.vue'
export { default as UiTextarea } from '../../components/UiTextarea.vue'

export const LazyUiButton = import('../../components/UiButton.vue' /* webpackChunkName: "components/UiButton" */).then(c => c.default || c)
export const LazyUiDataTable = import('../../components/UiDataTable.vue' /* webpackChunkName: "components/UiDataTable" */).then(c => c.default || c)
export const LazyUiInputField = import('../../components/UiInputField.vue' /* webpackChunkName: "components/UiInputField" */).then(c => c.default || c)
export const LazyUiPaginator = import('../../components/UiPaginator.vue' /* webpackChunkName: "components/UiPaginator" */).then(c => c.default || c)
export const LazyUiTextarea = import('../../components/UiTextarea.vue' /* webpackChunkName: "components/UiTextarea" */).then(c => c.default || c)
