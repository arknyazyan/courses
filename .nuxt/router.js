import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _296457ff = () => interopDefault(import('../pages/courses/index.vue' /* webpackChunkName: "pages/courses/index" */))
const _465da4de = () => interopDefault(import('../pages/courses/add/index.vue' /* webpackChunkName: "pages/courses/add/index" */))
const _135cfcac = () => interopDefault(import('../pages/courses/course/_id/index.vue' /* webpackChunkName: "pages/courses/course/_id/index" */))
const _176aa196 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/courses",
    component: _296457ff,
    name: "courses"
  }, {
    path: "/courses/add",
    component: _465da4de,
    name: "courses-add"
  }, {
    path: "/courses/course/:id",
    component: _135cfcac,
    name: "courses-course-id"
  }, {
    path: "/",
    component: _176aa196,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
