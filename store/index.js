export const state = () => ({
  courses: [],
});
export const mutations = {
  INIT(state, data) {
    state.courses = [...data];
  },

  CREATE(state, data) {
    data.id = state.courses.length
      ? Math.max(...state.courses.map((e) => e.id)) + 1
      : 1;
    state.courses.push(data);
  },

  UPDATE(state, data) {
    const index = state.courses.findIndex((e) => e.id === data.id);
    state.courses[index] = data;
  },

  DELETE(state, id) {
    const index = state.courses.findIndex((e) => e.id === id);
    state.courses.splice(index, 1);
  },
};
export const actions = () => ({});
